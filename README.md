# TestApp Docs

This repository contains the MkDocs-based documentation of [TestApp](https://gitlab.com/testapp-system/).

## Getting started

### Install the dependencies

```bash
python -m virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
```
### Run and build

```bash
# For debugging
mkdocs serve
# For release build
mkdocs build
```

## License

All parts of TestApp including this one are licensed under the terms and conditions of the `EUPL-1.2`.
