# Info

Auf dieser Seite werden Links zu verschiedenen Versionen der TestApp angezeigt und hier befindet sich das Impressum sowie die Datenschutzerklärung.

Die Links können in der [Einführung](../index.md) gefunden werden.

## Impressum

Impressum gemäß der deutschen Impressumspflicht

Verantwortlicher und Seitenbetreiber ist:

!!! info
    Jasper Michalke  
    August-Kirch-Str. 15j  
    D-22525 Hamburg  
    GERMANY  

    Mail-Adresse: info@testapp.ga  
    Telefon: +491704865407

## Datenschutzerklärung

Usere Datenschutzerklärung kann zusätzlich auf [GitLab](https://gitlab.com/testapp-system/testapp-flutter/-/blob/mobile/PRIVACY.md) abgerufen werden.