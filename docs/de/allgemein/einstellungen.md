# Einstellungen

In den Einstellunge kannst du die TestApp für dich verwalten.

## Account

Hier kannst du dich abmelden oder dein Passwort ändern

## Sprache

Hier kannst du die Anzeigesprache der TestApp festlegen. Derzeit unterstützt werden:

 - Englisch
 - Deutsch
 - Französisch
 - Klingonisch
