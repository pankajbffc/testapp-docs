# Die erste Benutzung

Wenn du die TestApp installiert hat und sie das erste Mal öffnest, kommst du auf die Login Seite. Hier kannst du dich mit deinen Zugangsdaten anmelden oder dich [registrieren](registrieren.md).

!!! info
    Lehrer sollten unbedingt die [Registrierungshinweise für Lehrer](../lehrer/lehrer-registrieren.md) lesen.

Nach der erfolgreichen Anmeldung kommst du zum [Dashboard](../schüler/dashboard.md).