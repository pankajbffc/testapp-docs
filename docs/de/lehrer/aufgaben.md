# Aufgaben

In der Aufgaben-Ansicht werden alle Aufgaben aufgelistet.

## Navigation

In der oberen Navigationsleiste kann zuerst das Fach ausgewählt werden. Das Fach kann auf Mobilgeräten ebenfalls durch horizontales Wischen gewechselt werden.

Die zentrale Liste zeigt die Themen des ausgewählten Faches. Durch Tippen auf das Thema werden die einzelnen Aufgaben eingeblendet.

## Aufgaben bearbeiten und erstellen

Um eine **Aufgabe zu bearbeiten**, den Bleistift am ende der Zeile drücken. Danach kommst du zur [Aufgabe bearbeiten](aufgabe-bearbeiten.md) Ansicht.

Um **Aufgaben zu erstellen** gibt es zwei Möglichkeiten, den [Aufgabe erstellen](aufgabe-bearbeiten.md) Dialog sowie die [Stapelverarbeitung](aufgaben-stapelverarbeitung.md). Letztere eignet sich besonders für Vokabelaufgaben.