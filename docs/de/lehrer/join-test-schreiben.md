# Join Test schreiben

Hier erfährst du schrittweise, wie du im Kursverbund einen Join Test erstellst und schreibst.

!!! info
    Wenn du **bereits einen Kurs und einen Join Test angelegt** hast und diesen nur noch schreiben möchtest, kannst du bei [Join Test schreiben](#test-schreiben) weiter lesen.

# Kurs anlegen

Da Join Tests im Kursverbund geschrieben werden, muss zu erst ein [Kurs angelegt](kurse.md##kurs-erstellen) werden, in dem alle Schüler\*innen eingetragen sind, die am Join Test teilnehmen sollen. Wie man neue Schüler\*innen in einen Kurs einschreibt, findest du unter [Kurs bearbeiten](kurse-bearbeiten.md).

!!! info
    Es ist zu empfehlen, unter [Kurse bearbeiten](kurse-bearbeiten.md) auch alle Themen, die der Join Test umfassen soll, für den gesamten Kurs auszuwählen, damit die [Scores der Schüler\*innen](kurse-bearbeiten.md#themen) besser verleichbar sind.

## Join Test erstellen

Als nächstes muss ein Join Test mit den richtigen Aufgaben [erstellt](join-tests.md#join-test-erstellen), [bearbeitet](kurse-bearbeiten.md) oder [abgezweigt](join-test-bearbeiten.md#test-abzweigen) werden. Danach muss dieser gespeichert werden.

## Test schreiben

!!! warn
    **Wichtig:** Falls der Join Test gerade bearbeitet wird, muss dieser erst gespeichert werden, damit die gerade vorgenommen Änderungen übernommen werden.

Durch Tippen auf `Diesen Test schreiben...` kann der Join Test dann geschreiben werden.

In dem folgenden Dialog muss der Kurs, die Bearbeitungszeit und die Anfangszeit ausgewäht werden. Letztere gibt an, in wie vielen Minuten vom jeweiligen Startzeitpunkt das Anfangen des Tests erlaubt ist.

!!! info
    Wenn um 09:00 auf `Test starten` getippt wird, können Schüler\*innen bei einer Anfangszeit von z.B. 30 Min. bis 09:30 dem Join Test beitreten. Dies beeinflusst nicht die Bearbeitungszeit des Join Tests.

Durch Tippen auf `Test starten` wird der eigentliche Test angefangen.

## Dem test beitreten

Nachdem der Test gestartet wurde, wird ein Infobildschirm zum Test angezeigt. Hier kann ein persönlicher Timer gestartet werden, um die Zeit im Auge zu behalten und die **Join Id** entnommen werden.

*Damit Schüler\*innen dem Test beitreten können, benötigen sie diese Join Id.*

Schüöer\*innen müssen diese, wie unter [Join Test](../schüler/join-test.md) beschrieben, eingeben un dann ihren Test starten.

Nachdem der Test fertig geschreiben ist, kann der Score des Tests unter [Kurs Score](kurse-bearbeiten.md#join-tests) eingesehen werden.