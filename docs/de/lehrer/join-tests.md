# Join Tests

Join Tests sind soetwas wie Klausuren, Schultests bzw. Klassenarbeiten. Sie werden im Kurssatz geschrieben und haben eine feste Bearbeitungszeit.

## Join Tests

In der zentralen Liste findest du alle Join Tests. Um einen Join Test zu [schreiben](join-test-schreiben.md) ober zu [bearbeiten](join-test-bearbeiten.md), auf diesen tippen. Dadurch kommtst du auf die [Join Test Bearbeiten](join-test-bearbeiten.md)-Seite.

## Join Test erstellen

Durch tippen auf :fa-plus: wird ein neuer Join Test erstellt und du kommst auf die [Join Test Bearbeiten](join-test-bearbeiten.md)-Seite.