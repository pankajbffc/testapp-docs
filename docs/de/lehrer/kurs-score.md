# Kurs Score

Hier wird der Score der Kurse angezeigt. Es gibt Statistiken pro Sch&uuml;ler\*in, nach [Thema](aufgabe-bearbeiten.md#themen), sowie f&uuml;r jeden [Join Test](join-tests.md).

## Welche Scores werden hier angezeigt

In der &Uuml;bersicht werden der durchschnittliche Score aller Aufgaben angezeigt, die die Sch&uuml;ler\*in in einem der f&uuml;r diesen Kurs ausgew&auml;hlten Themen in [Random Tests](../schüler/random-test.md) bearbeitet hat. Die Ergebnisse von [Join Tests](join-tests.md) werden nicht ber&uuml;cksichtigt.

!!! warn
    **Achtung:** Wenn dem Kurs keine oder die falschen [Themen zugeordnet](kurse-bearbeiten.md) werden, kann es sein, dass gar keine, falsche oder unvollst&auml;ndige Scores der Sch&uuml;ler\*innen angezeigt werden.

## &Uuml;berblick

In diesem Tab wird der allgemeine Leistungsstand des Kurses angezeigt:

 - Der durchschnittliche Score des gesamten Kurses, sowie
 - der durchschnittliche Score aller einzelnen Sch&uuml;ler\*innen

jeweils des letzten halben Jahres.

## Themen

In dieser Liste werden f&uuml;r jedes gew&auml;hlte Thema die Scores angezeigt. Hierbei wird wieder der durchschnittlich Score des Themas im Bezug auf den gesamten Kurs sowie pro Sch&uuml;ler\*in angezeigt.

Am Beginn der Zeilen steht jeweils das Fach gefolgt vom Thema selbst. Durch Tippen auf eine Zeile wird der Score des jeweiligen Themas angezeigt.

## Join Tests

Hier werden alle mit dem Kurs geschriebenen [Join Tests](join-tests.md) angezeigt.

Am Beginn der Zeile steht, wann der Join Test geschrieben wurde gefolgt vom Namen des Join Tests. Durch Tippen auf die Zeile wird der Score des Join Tests angezeigt.

Der durchschnittliche Score wird zuerst angezeigt. Darunter befindet sich eine Liste der Scores aller Sch&uuml;ler\*innen, die am Join Test teilgenommen haben. Die genauen Antworten der Sch&uuml;ler\*innen k&ouml;nnen durch Tippen auf den die jeweilige Zeile eingesehen werden. Mehr dazu unter [Test Score](../schüler/test-score.md).