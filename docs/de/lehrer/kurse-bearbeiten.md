# Kurs bearbeiten

Hier k&ouml;nnen [Kurse](kurse.md) bearbeitet oder gel&ouml;scht werden.

## Kurs umbenennen

Oben auf der Seite k&ouml;nnen Kurse umbenannt werden. Dazu muss einfach der angezeigte Name ge&auml;ndert werden.

!!! info
    **Hinweis:** Denk über den Namen nach. Manchmal sind Namen wie `10b` sinnvoll, manchmal aber auch Namen wie `10b/Chemie`. Sprich dich am besten mit deinen Kolleg\*innen ab.

## Kurs löschen

Durch Tippen auf das :fa-trash:-Symbol in der oberen rechten Ecke, kann der aktuelle Kurs gel&ouml;scht werden.

## Themen

Jedem Kurs m&uuml;ssen [Themen](aufgabe-bearbeiten.md#themen) zugeordnet werden. Zu den jeweiligen Themen werden die Ergebnisse der Sch&uuml;ler\*innen gesammelt.

!!! warn
    **Achtung:** Wenn einem Kurs keine oder die falschen Themen zugeordnet werden, kann es sein, dass gar keine, falsche oder unvollst&auml;ndige Ergebnisse der Sch&uuml;ler\*innen unter [Kurs Score](kurs-score.md) angezeigt werden.

## Sch&uuml;ler\*innen

Hier können die Sch&uuml;ler\*innen des Kurses bearbeitet werden. Von allen Sch&uuml;ler\*innen, die hier ausgew&auml;hlt sind, wird der [Score](kurs-score.md) sp&auml;ter angezeigt.

Die Sch&uuml;ler\*innen sind nach dem Vornamen alphabetisch sortiert. Sie werden durch Tippen auf die Zeile des Namens ausgew&auml;hlt.