# Kurse

In der TestApp kannst du als Lehrer\*in Kurse anlegen, um die Scores deiner Schüler\*innen zu sehen oder im Kurssatz [Join Tests](join-tests.md) zu schreiben. Kurse k&ouml;nnen zum Beispiel Sch*uuml;lergruppen oder Klassen (Schulklassen) sein.

## Meine Kurse

Unter dieser Liste findest du alle von dir angelegten Kurse. Wenn du auf einen der Kurse tippst, gelangst du zum [Kurs-Score] dieses Kurses. Wenn du auf :fa-pencil: tippst, kannst du den [Kurs Bearbeiten](kurse-bearbeiten.md).

## Kurs erstellen

Wenn du auf :fa-plus: tippst, kannst du einen neuen Kurs erstellen.

Gib dazu als erstes einen Namen für den Kurs ein.

!!! info
    **Hinweis:** Denk über den Namen nach. Manchmal sind Namen wie `10b` sinnvoll, manchmal aber auch Namen wie `10b/Chemie`. Sprich dich am besten mit deinen Kolleg\*innen ab.

Durch tippen auf `Kurs erstellen` wird der Kurs erstellt und du kannst Schüler*innen und Themen hinzufügen. Lies dazu [Kurs bearbeiten](kurse-bearbeiten.md).