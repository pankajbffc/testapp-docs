# What is TestApp

[TestApp](https://testapp.ga/) is an educational open-source project for **writing class tests digitally** on smartphones or computers. We offer **easy-to-use class management**, **individual** class tests and random **practice** tests. Students can register **individually** or a **hole school/class** can use TestApp.

### Technical Goals

* **Statistics** for both students and teachers
* **Motivating** UI/UX
* Cross-platform
* **Mobile** apps, **Desktop** version and **Web** app
* **Secure** and **privacy** friendly
* *100% Open Source*
* Create scientific exercises using **LaTex** and **RegEx**
* Create **vocabulary** questions with multiple correct answers
* Support for **multiple choice** exercises
* **Random tests** and **class tests**

### Apps

* Windows 10: [Microsoft Store](https://www.microsoft.com/en-us/p/testapp-system/9p9jjfmff77d)
* Android:
    - [Play Store](https://play.google.com/store/apps/details?id=ga.testapp.testapp)
    - [F-Droid](https://f-droid.org/en/packages/ga.testapp.testapp/)
* iOS: [App Store](https://apps.apple.com/us/app/testapp-system/id1490425513)
* Desktop:
    - [Website](https://testapp.ga/static/downloads.html)
    - [GitLab](https://gitlab.com/testapp-system/testapp-electron/pipelines)
    - Linux: [Snap Store](https://snapcraft.io/testapp-desktop)
* Chrome OS: [Web Store](https://chrome.google.com/webstore/detail/testapp-system/hclopnbfffconajgdcmibjekjhmfegjf)

### Support development

You can support us by coding. Just ask to become member of [GitLab TestApp group](https://gitlab.com/testapp-system).

*For running our servers and deploying mobile apps we need $200. Help us!*

Feel free to donate: [Buy me a coffee](https://www.buymeacoffee.com/JasMich) or BTC: *3NUiJXDCkyRTb9Tg7n63yK6Y7CexADtSEh*

## License

TestApp is licensed under the `EUPL-1.2`.
