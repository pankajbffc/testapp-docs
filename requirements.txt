mkdocs==1.1
mkdocs-git-revision-date-localized-plugin==0.4.8
mkdocs-material==4.6.3
fontawesome-markdown==0.2.6
mkdocs-minify-plugin==0.2.3
